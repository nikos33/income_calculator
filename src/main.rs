fn process_fat_accounts(accounts: &mut [f32], base: f32) -> f32 {
    let mut sum = 0_f32;
    for i in 0..accounts.len() {
        let money = accounts[i];
        if money > base {
            let f = (money / base).powf(1.3);
            if accounts[i] - f > base {
                accounts[i] -= f;
                sum += f;
            }
        }
    }
    sum
}

fn process_poor_accounts(accounts: &mut [f32], base: f32, benefits: f32) {
    let small_amount_limit = |x: f32| -> f32 {
        if x > 20_f32 {
            20_f32
        } else {
            x
        }
    };

    //let all_factors = accounts.into_iter().filter(|&s| *s < base).fold(0f32, |sum, val| sum + small_amount_limit(base / *val));
    
    let mut all_factors = 0_f32;
    //for mm in accounts {
    for i in 0..accounts.len() {
        let mm = accounts[i];    
        if mm < base{
            all_factors += small_amount_limit(base / mm);
        }
    }

    let amount_to_add = benefits / all_factors;

    for i in 0..accounts.len() {
        let money = accounts[i];
        if money < base {
            let f = small_amount_limit(base / money) * amount_to_add;
            accounts[i] += f;
        }
    }
}
fn main() {
    //let mut accounts: Vec<f32> = Vec::with_capacity(100000);
    //let mut accounts = vec![0_f32; 10000];
    let mut accounts = [0_f32; 100000];
     for i in 0..100000 {
         accounts[i] = (i*2) as f32;
     }


    let mut accounts2 = "50000 800 300 70 55 35 24 18 14 10 10 10 10 8 8 8 8 6"
        .split(" ")
        .filter_map(|s| s.parse::<f32>().ok())
        .collect::<Vec<_>>();

    let base: f32 = accounts.iter().fold(0f32, |sum, val| sum + val) / accounts.len() as f32;
    println!("base: {:.2}", base);

    for i in 0..100 {
        let benefits = process_fat_accounts(&mut accounts, base);
        process_poor_accounts(&mut accounts, base, benefits);
        //println!("[ {}] счета {:.2?} снято с  жирных счетов #{:.2}", i, accounts.iter().take(10), benefits);
        println!("[ {}] снято с  жирных счетов #{:.2}", i, benefits);
        //accounts.iter().fold(String::new(), |acc, &num| acc + &num.to_string() + ", ")

    }
    println!("processed:");
}
